# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from psolve.authentication.models import Account

admin.register(Account)
