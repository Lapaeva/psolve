# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models


class AccountManager(UserManager):
    def create_user(self, username, password=None, email=None, **kwargs):
        if not username:
            raise ValueError('Users must have a valid username.')

        account = self.model(
            username=username
        )
        account.is_active = True
        account.set_password(password)
        account.save()

        return account

    def create_superuser(self, username, password, **kwargs):
        account = self.create_user(username, password, **kwargs)

        account.is_staff = True
        account.is_admin = True
        account.is_superuser = True

        account.save()

        return account


class Account(AbstractUser):
    email = models.EmailField(blank=True, null=True)
    username = models.CharField(max_length=40, unique=True)

    first_name = models.CharField(max_length=40, blank=True)
    last_name = models.CharField(max_length=40, blank=True)
    tagline = models.CharField(max_length=140, blank=True)

    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = AccountManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['password']

    def __unicode__(self):
        return self.username

    def get_full_name(self):
        return ' '.join([self.first_name, self.last_name])

    def get_short_name(self):
        return self.first_name

    # simple  password check
    def check_password(self, raw_password):
        if raw_password == self.password:
            return True
        return super(Account, self).check_password(raw_password)

    def tojson(self):
        return {'username': self.username, 'password': self.password, 'firstName': self.first_name,
                'lastName': self.last_name}
