# -*- coding: utf-8 -*-
import uuid
import jwt

from django.contrib.auth import authenticate, login, logout
from django.conf.urls import url
from tastypie import fields
from tastypie.http import HttpUnauthorized, HttpForbidden
from tastypie.utils import trailing_slash

from psolve.core.api_base import BaseResource
from psolve.authentication.models import Account
from psolve.core.settings import SECRET_KEY


class AccountResource(BaseResource):
    username = fields.CharField(attribute='username')
    password = fields.CharField(attribute='password')
    firstName = fields.CharField(attribute='first_name')
    lastName = fields.CharField(attribute='last_name')

    class Meta(BaseResource.Meta):
        queryset = Account.objects.all()
        resource_name = 'account'
        excludes = ('password', 'is_superuser')

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/login%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name="api_login"),
            url(r'^(?P<resource_name>%s)/logout%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('logout'), name='api_logout'),
        ]

    # we need this method for getting a token for user
    def login(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        data = self.deserialize(request, request.body,
                                format=request.META.get('Content_Type', 'application/json'))

        username = data.get('username', '')
        password = data.get('password', '')

        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                token = jwt.encode({'username': username, 'password': password}, SECRET_KEY, algorithm='HS256')
                user_with_token = user.tojson()
                user_with_token['token']=token
                return self.create_response(request, {
                    'success': True,
                    'user': user_with_token
                })
            else:
                return self.create_response(request, {
                    'success': False,
                    'reason': 'disabled',
                }, HttpForbidden)
        else:
            return self.create_response(request, {
                'success': False,
                'reason': 'incorrect',
            }, HttpUnauthorized)

    def logout(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        if request.user and request.user.is_authenticated():
            logout(request)
            return self.create_response(request, {'success': True})
        else:
            return self.create_response(request, {'success': False}, HttpUnauthorized)