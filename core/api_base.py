import jwt
from django.contrib.auth import authenticate

from tastypie.authorization import Authorization
from tastypie.authentication import Authentication
from tastypie.serializers import Serializer
from psolve.core.cors import CORSModelResource

from psolve.core.settings import SECRET_KEY


class JWTAuthentication(Authentication):
    auth_type = 'bearer'

    def is_authenticated(self, request, **kwargs):
        token = super(JWTAuthentication, self).get_authorization_data(request)
        if token:
            payload = jwt.decode(token, SECRET_KEY)
            if all(k in payload for k in ("username", "password")):
                user = authenticate(username=payload['username'], password=payload['password'])
                request.user = user
        return False


class BaseResource(CORSModelResource):
    class Meta():
        authentication = JWTAuthentication()
        authorization = Authorization()
        serializer = Serializer(formats=('json',))
        allowed_methods = ('get', 'post', 'put', 'patch')
        ordering = ('id',)
        max_limit = None
        always_return_data = True