import json

from tastypie.exceptions import TastypieError
from tastypie.http import HttpBadRequest


class ErrorRequest(TastypieError):
    def __init__(self, field="", message=""):
        self._response = {"error": {"field": field, "message": message}}

    @property
    def response(self):
        return HttpBadRequest(json.dumps(self._response), content_type='application/json')
