# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Problem, UserAnswers

class ProblemAdmin(admin.ModelAdmin):
    list_display = ('problem', 'right_answer')

admin.site.register(Problem, ProblemAdmin)
admin.site.register(UserAnswers)

