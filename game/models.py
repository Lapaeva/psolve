# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from psolve.authentication.models import Account


class Problem(models.Model):
    problem = models.CharField(max_length=250)
    right_answer = models.CharField(max_length=100)  # can we save to_lower without any other actions?

    def __unicode__(self):
        return self.problem

    # implement simple check
    def check_answer(self, answer):
        if self.right_answer==answer:
            return True
        return False


class UserAnswers(models.Model):
    user = models.ForeignKey(Account)
    answers_stack = models.ManyToManyField(Problem)  # correct user's answers
