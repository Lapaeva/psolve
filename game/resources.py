# -*- coding: utf-8 -*-
from tastypie import fields

from psolve.game.models import UserAnswers, Problem
from psolve.authentication.models import Account
from psolve.core.api_base import BaseResource
from psolve.core.exception import ErrorRequest


class ProblemResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = Problem.objects.all()
        resource_name = 'Problem'
        excludes = ['right_answer', ]  # do not show client the right answer


class UserAnswerResource(BaseResource):
    answers_stack = fields.ToManyField(ProblemResource, 'answers_stack', full=True, blank=True, null=True)

    class Meta(BaseResource.Meta):
        queryset = UserAnswers.objects.all()
        resource_name = 'UserAnswer'
        allowed_methods = ['get', 'post', ]

    def get_problem_from_request(self, body):
        if not body.has_key("problem") and \
                not body["problem"].has_key("answer") and \
                not body["problem"].has_key("id"):
            raise ErrorRequest(field='problem', message='Please provide correct Problem with id and answer')

        problem = Problem.objects.filter(pk=body['problem']['id']).first()
        if not problem:
            raise ErrorRequest(field='problem', message='Problem with provided id is not exist')
        return problem

    def post_detail(self, request, **kwargs):
        if not kwargs.has_key('pk'):
            raise ErrorRequest(field='pk', message='Pk should be provided in url')

        user_answer = UserAnswers.objects.get(pk=kwargs['pk'])
        if not user_answer:
            raise ErrorRequest(field='pk', message='There is no UserAnwser with this pk')

        deserialized = self.deserialize(request, request.body,
                                        format=request.META.get('CONTENT_TYPE', 'application/json'))
        problem = self.get_problem_from_request(deserialized)

        answer_from_user = deserialized["problem"]["answer"]

        # implements simple check
        if problem.check_answer(answer_from_user):
            user_answer.answers_stack.add(problem)
        else:
            last_answer = user_answer.answers_stack.last()
            if last_answer:
                user_answer.answers_stack.remove(last_answer.pk)
        user_answer.save()

        bundle = self.build_bundle(request=request, obj=user_answer)

        updated_bundle = self.full_dehydrate(bundle)
        updated_bundle = self.alter_detail_data_to_serialize(request, updated_bundle)
        return self.create_response(request, updated_bundle)

    # save UserAnswer only after first response from user
    def obj_create(self, bundle, **kwargs):
        user = Account.objects.get(username=bundle.request.user.username)

        problem = self.get_problem_from_request(bundle.data)

        answer_from_user = bundle.data["problem"]["answer"]

        bundle = super(UserAnswerResource, self).obj_create(bundle, user=user)

        # implements simple check
        if problem.check_answer(answer_from_user):
            bundle.obj.answers_stack.add(problem)
        bundle.obj.save()

        return bundle
