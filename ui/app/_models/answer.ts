import { Task } from './task'
import { User } from './user'

export class Answer {
    id: number;
    resource_uri: string;
    problem: Task;
    user: User;
}