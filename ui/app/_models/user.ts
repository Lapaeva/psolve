﻿export class User {
    id: number;
    resource_uri: string
    username: string;
    password: string;
    firstName: string;
    lastName: string;
}