﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { URL } from '../settings'
import { Options } from './headers'
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    constructor(private http: Http) { }

    login(username: string, password: string) {
        let apiURL = URL+'api/v1/account/login/';
        return this.http.post(apiURL, JSON.stringify({ username: username, password: password }), Options())
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let data = response.json();
                let user = data.user;
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                   localStorage.setItem('currentUser', JSON.stringify(user));
                }
            });
    }

    logout() {
        return this.http.get(URL+'api/v1/account/logout/', Options())
            .map((response: Response) => {
               localStorage.removeItem('currentUser'); // remove user from local storage
            });
    }
}