import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Task, Answer } from '../_models/index';
import { URL } from '../settings'
import { Options } from './headers'

@Injectable()
export class GameService {
    constructor(private http: Http) { }

    getNextTask(){
        let url =  URL + 'api/v1/Problem/';
        return this.http.get(url, Options()).map((response: Response) => response.json().objects);
    }

    submitUserAnswer(answer: Answer){
        let  id = ''
        if (answer.id){
            id = answer.id+'/'
        }
        return this.http.post(URL + 'api/v1/UserAnswer/'+id, JSON.stringify(answer), Options() ).map((response: Response) => response.json())
    }
}