import { Http, Headers, RequestOptions } from '@angular/http';

export function Options(){
    let _headers = {'Content-Type': 'application/json'};
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
        _headers['Authorization'] = 'Bearer ' + currentUser.token;
    }
    return new RequestOptions({ headers: new Headers(_headers) });
}