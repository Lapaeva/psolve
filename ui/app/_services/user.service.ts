﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { User } from '../_models/index';
import { URL } from '../settings';
import { Options } from './headers'

@Injectable()
export class UserService {
    constructor(private http: Http) { }
    private apiURL = URL + '/api/v1/account/';

    getAll() {
        return this.http.get(this.apiURL, Options()).map((response: Response) => response.json());
    }

    getById(id: number) {
        return this.http.get(this.apiURL + id, Options()).map((response: Response) => response.json());
    }

    create(user: User) {
        return this.http.post(this.apiURL, JSON.stringify(user), Options()).map((response: Response) => response.json());
    }

    update(user: User) {
        return this.http.put(this.apiURL + user.id, user,  Options()).map((response: Response) => response.json());
    }

    delete(id: number) {
        return this.http.delete(this.apiURL + id,  Options()).map((response: Response) => response.json());
    }
}