import { Component, OnInit } from '@angular/core';

import { User, Task, Answer } from '../_models/index';
import { GameService, AlertService } from '../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'game.component.html'
})

export class GameComponent implements OnInit {
    currentUser: User;
    task: Task=new Task();
    tasks: Task[];
    loading = false;
    model: any = {};
    answer: Answer = new Answer();
    answers_stack = new Array<Task>();

    constructor(private gameService: GameService,
                private alertService: AlertService){
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit(){
       this.getTasks();
    }

    submit(){
        this.answer.user = this.currentUser;
        this.answer.problem = this.task;
        this.answer.problem.answer = this.model.answer;
        this.gameService.submitUserAnswer(this.answer).subscribe(
            data=>{
                this.answer.id = data.id;
                this.answers_stack = data.answers_stack;
                this.model.answer='';
                if (this.tasks){
                    this.task = this.tasks.pop()
                }
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }

    getTasks(){
        this.loading = true;
        this.gameService.getNextTask().subscribe(
            data => {
                this.tasks = data;
                this.loading = false;
                this.task = this.tasks.pop();
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}