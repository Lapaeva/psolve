﻿import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';

import { User, Answer } from '../_models/index';
import { GameService } from '../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {
    currentUser: User;
    answers: Answer[];

    constructor(private gameService: GameService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {}
}